<?php

/**
 * last updated 06-April-2022
 *
 */

namespace Apps\Payfast\Block;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Element\Template\Context;
use Magento\Checkout\Model\Session;
use Magento\Customer\Model\SessionFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;
use Apps\Payfast\Logger\Logger;
use Magento\Framework\App\Response\Http;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Sales\Model\Order\Payment\Transaction\Builder as TransactionBuilder;

class Main extends \Magento\Framework\View\Element\Template
{
    protected $_objectmanager;
    protected $checkoutSession;
    protected $orderFactory;
    protected $urlBuilder;
    private $logger;
    protected $response;
    protected $config;
    protected $messageManager;
    protected $transactionBuilder;
    protected $inbox;
    protected $_customerSession;
    protected $_addressRepo;
    public $publicConfig;
    public $authToken;
    public $basketId;
    public $orderAmount;
    public $mobileNumber;
    public $email;
    public $customerData = [];
    public $successUrl;
    public $failureUrl;
    public $ipnResponseUrl;
    public $currency_code;
    private $_tokenUrl = '/Ecommerce/api/Transaction/GetAccessToken';
    private $_webcheckoutUrl = '/Ecommerce/api/Transaction/PostTransaction';
    protected $_storeManager;
    public $store_id;
    public $payfast_merchant_id;
    public $store_merchant_name;
    public $ipnCheckoutUrl;
    public $is_guest;
    public $customer_name;
    public $customer_ip;
    public $country_id;
    public $itemsList = [];
    public $shipping_customer_name;
    public $shipping_address_1;
    public $shipping_address_2;
    public $shipping_city;
    public $shipping_region;
    public $shipping_postcode;
    public $shipping_method;
    public $shipping_country;
    public $billing_customer_name;
    public $billing_address_1;
    public $billing_address_2;
    public $billing_city;
    public $billing_region;
    public $billing_postcode;
    public $billing_country;

    public function __construct(
        Context $context,
        Session $checkoutSession,
        OrderFactory $orderFactory,
        Http $response,
        TransactionBuilder $tb,
        SessionFactory $customerSession,
        AddressRepositoryInterface $addressRepo
    ) {
        $this->_storeManager = $context->getStoreManager();
        $this->checkoutSession = $checkoutSession;
        $this->orderFactory = $orderFactory;
        $this->response = $response;
        $this->config = $context->getScopeConfig();
        $this->transactionBuilder = $tb;
        $this->publicConfig = $this->config;
        $this->urlBuilder = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\UrlInterface');
        $this->_customerSession = $customerSession->create();
        $this->_addressRepo = $addressRepo;

        parent::__construct($context);
    }

    protected function _prepareLayout()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logger = $objectManager->create('\Apps\Payfast\Logger\Logger');

        $method_data = array();

        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $merchant_id = $this->publicConfig->getValue("payment/payfast/payfast_merchant_id", $storeScope);
        $this->payfast_merchant_id = $merchant_id;
        $secured_key = $this->publicConfig->getValue("payment/payfast/payfast_secured_key", $storeScope);
        $this->store_id = $this->publicConfig->getValue("payment/payfast/payfast_store_id", $storeScope);
        $this->store_merchant_name = $this->getStoreName();

        $txnBaseUrl = $this->publicConfig->getValue("payment/payfast/payfast_txn_base_url", $storeScope);

        $this->_tokenUrl = $txnBaseUrl . $this->_tokenUrl;

        $this->_webcheckoutUrl = $txnBaseUrl . $this->_webcheckoutUrl;

        $logger->info('IPG Token URL: ' . $this->_tokenUrl);

        $logger->info('Checkout URL: ' . $this->_webcheckoutUrl);

        $order = $this->checkoutSession->getLastRealOrder();
        $incrementId = $order->getIncrementId();
        $order = $this->orderFactory->create()->loadByIncrementId($incrementId);
        $orderId = $incrementId;
        $this->basketId = $incrementId;

        if ($order) {
            $currentStatus = $order->getStatus();
            $currentState = $order->getState();
            if ($currentState !== Order::STATE_NEW && $currentStatus  !== Order::STATE_PENDING_PAYMENT) {
                $redirectUrl = $this->urlBuilder->getUrl('checkout/cart', ['_secure' => true]);
                echo sprintf('<meta http-equiv="refresh" content="0; url=%s">', $redirectUrl);
                return;
            }
        }

        if ($this->_customerSession->isLoggedIn()) {
            $this->customerData['email'] = $this->_customerSession->getCustomer()->getEmail();
        } else {
            $this->customerData['email'] = $order->getBillingAddress()->getEmail();
        }


        $_ShippingObject = $order->getShippingAddress();
        $_BillingObject = $order->getBillingAddress();
        $this->customerData['mobile'] = '';
        
        if ($_ShippingObject) {
            $mobile_number = $_ShippingObject->getData('telephone');
            $this->customerData['mobile'] = $mobile_number;
        } else {
            $this->customerData['mobile'] = '';
        }

        $this->currency_code = $order->getOrderCurrencyCode();

        $this->responseUrl  = $this->urlBuilder->getUrl("payfast/response");
        $this->responseUrl = $this->responseUrl . '?ref=m2&redirect=Y';
        $this->ipnResponseUrl = $this->urlBuilder->getUrl("payfast/ipn");

        $billingAddress = $order->getBillingAddress()->toArray();
        $shippingAddress = $order->getShippingAddress()->toArray();


        $this->shipping_address_1 = $shippingAddress['street'];
        $this->shipping_address_2 = '';
        $this->shipping_region = $shippingAddress['region'];
        $this->shipping_postcode = $shippingAddress['postcode'];
        $this->shipping_city = $shippingAddress['city'];
        $this->shipping_country = $shippingAddress['country_id'];
        $this->shipping_method = $order->getShippingMethod();


        $this->billing_address_1 = $billingAddress['street'];
        $this->billing_address_2 = '';
        $this->billing_region = $billingAddress['region'];
        $this->billing_postcode = $billingAddress['postcode'];
        $this->billing_city = $billingAddress['city'];
        $this->billing_country = $billingAddress['country_id'];

        $this->customer_ip =  $order->getRemoteIp();

        $this->customer_name = sprintf(
            "%s %s %s",
            $order->getCustomerFirstname(),
            $order->getCustomerMiddlename(),
            $order->getCustomerLastname()
        );

        $purchasedItems = $order->getAllVisibleItems();
        foreach ($purchasedItems as $singleItem) {
            $itemDetail = $singleItem->toArray();
            $itemObjectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productNew = $itemObjectManager->create('Magento\Catalog\Model\Product')->load($itemDetail['product_id']);

            $product = array(
                'id' => $itemDetail['product_id'],
                'name' => $itemDetail['name'],
                'price' => $itemDetail['price_incl_tax'],
                'qty' => $itemDetail['qty_ordered'],
                'sku' => $productNew->getSku()
            );

            $this->itemsList[] = $product;
        }


        if ($order) {
            $payment = $order->getPayment();
            $order->setState(Order::STATE_NEW)->setStatus(Order::STATE_PENDING_PAYMENT);
            $this->orderAmount = $order->getGrandTotal();
            $this->getPayfastAuthToken($merchant_id, $secured_key, $this->orderAmount, $orderId, $this->currency_code);
            $payment->setCurrencyCode($this->currency_code);
            $payment->save();
            $order->save();
        }
    }

    private function getPayfastAuthToken($merchant_id, $secured_key, $amount, $basket_id, $currency)
    {
        $postFields = sprintf(
            'MERCHANT_ID=%d&SECURED_KEY=%s&TXNAMT=%s&BASKET_ID=%s&CURRENCY_CODE=%s',
            $merchant_id,
            $secured_key,
            $amount,
            $basket_id,
            $currency
        );
        $this->authToken = $this->curl_request($this->_tokenUrl, $postFields);
    }

    private function getCustomerData()
    {
        if ($this->_customerSession->isLoggedIn()) {
            $this->customerData['email'] = $this->_customerSession->getCustomer()->getEmail();
        } else {
            $this->customerData['email'] = "customer@example.com";
        }
    }

    private function curl_request($url, $postFields)
    {
        $certificate = __DIR__ . "/cacert.pem";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CAINFO, $certificate);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            $postFields
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'application/json'
        ));
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-CURL Addon Magento 2.4 PayFast');

        $response = curl_exec($ch);

        curl_close($ch);
        $response_decode = json_decode($response);

        if (isset($response_decode->ACCESS_TOKEN)) {
            return $response_decode->ACCESS_TOKEN;
        }
        return;
    }

    public function getAction()
    {
        return $this->_webcheckoutUrl;
    }

    public function getStoreName()
    {
        return $this->_storeManager->getStore()->getName();
    }
}
