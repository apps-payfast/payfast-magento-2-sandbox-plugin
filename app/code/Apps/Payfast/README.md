## PayFast Payment Gateway for Magento 2

Enable PayFast as a payment method in Magento 2

How to Install:
==============

- Extract the .zip package in Magento 2 Root
- Enable the module in Magento Module Manager
- Go to Magento Admin/Configurations/Sales/Payment Method and Select PayFast and provide the necessary configurations


Developed by Tech Team

http://gopayfast.com

info@gopayfast.com