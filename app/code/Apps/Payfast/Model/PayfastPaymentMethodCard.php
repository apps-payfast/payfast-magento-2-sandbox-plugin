<?php

namespace Apps\Payfast\Model;

use Apps\Payfast\Model\PayFastAPI\PayFastApi;

class PayfastPaymentMethodCard extends \Magento\Payment\Model\Method\AbstractMethod
{
    protected $_isInitializeNeeded = false;
    protected $redirect_uri;
    protected $_code = 'payfast_card';
    protected $_canOrder = true;
    protected $_isGateway = true;
    protected $_canRefund = true;
    protected $_canRefundInvoicePartial = true;


    public function getOrderPlaceRedirectUrl()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\UrlInterface')->getUrl("payfast/redirect");
    }

    public function refund(
        \Magento\Payment\Model\InfoInterface $payment,
        $amount
    ) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logger = $objectManager->create('\Apps\Payfast\Logger\Logger');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $context = $objectManager->create('Magento\Framework\View\Element\Template\Context');
        $storeManager = $context->getStoreManager();
        $baseCurrencyCode = $storeManager->getStore()->getBaseCurrencyCode();


        $transactionId = $payment->getParentTransactionId();
        $order = $payment->getOrder();
        $txnAmount = $order->getGrandTotal();
        $txnBaseTotal = $order->getBaseGrandTotal();
        $orderCurrency = $order->getOrderCurrencyCode();
        $orderId = $order->getIncrementId();
        $orderCurrency  = $order->getOrderCurrencyCode();

        $logMessage = sprintf(
            "Order ID: %s. Orginal Amount: %s %s. Amount to be refunded: %s %s. ",
            $orderId,
            $orderCurrency,
            $txnAmount,
            $baseCurrencyCode,
            $amount
        );

        $logger->info($logMessage);

        $isPartialRefund = $this->_checkPartialRefund($amount, $txnBaseTotal);

        if (!$this->_canRefundInvoicePartial && $isPartialRefund) {
            throw new \Magento\Framework\Exception\LocalizedException(__("Partial refund not allowed for this payment method."));
        }

        $blockCase = $this->_checkRefundBlockCases($amount, $txnBaseTotal, $payment);

        if ($blockCase) {
            throw new \Magento\Framework\Exception\LocalizedException(__("Invalid amount given for refund"));
        }

        $refundingAmount = $amount;
        $refundingCurrency = $baseCurrencyCode;

        if (!$isPartialRefund) {
            $refundingAmount = $txnAmount;
            $refundingCurrency = $orderCurrency;
        }

        $refundingAmount = number_format($refundingAmount, 2,'.','');

        $logger->info("\$refundingAmount: $refundingAmount");

        require __DIR__ . "/PayFastAPI/PayFastApi.php";

        try {
            $payfastApi = new PayFastApi();
            $apiResponse = $payfastApi->refundTransaction($transactionId, $refundingAmount, $refundingCurrency);
        } catch (\Magento\Framework\Exception\LocalizedException $exception) {
            $logger->info($payfastApi->getError());
            throw new \Magento\Framework\Exception\LocalizedException(__($exception->getMessage()));
        }

        if (!$apiResponse) {
            throw new \Magento\Framework\Exception\LocalizedException(__($refundError));
        }

        $refundTxnId = $payfastApi->getRefundTransactionId();
        if ($refundTxnId) {
            $logger->info('Tranaction ID: ' . $refundTxnId);
            $this->setRefundTransaction($order, $refundTxnId, $payment, $transactionId);
        }



        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $messageManager = $objectManager->create('\Magento\Framework\Message\ManagerInterface');
        $messageManager->addSuccess(__("Refund completed at PayFast"));
        $logger->info('Refund Completed: ' . $order->getIncrementId());

        return $this;
    }

    private function _checkRefundBlockCases($amount, $txnAmount, $payment)
    {
        switch ($amount) {
            case !$amount || $amount <= 0:
                return true;
            case $amount > $txnAmount:
                return true;
        }

        return false;
    }

    private function _checkPartialRefund($amount, $transactionAmount)
    {
        if ($amount < $transactionAmount) {
            return true;
        }

        return false;
    }

    private function setRefundTransaction($order, $transactionId, $payment, $originalTransactionId)
    {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $transactionBuilderObject = $objectManager->create('Magento\Sales\Model\Order\Payment\Transaction\Builder');

        $transactionBuilder = $transactionBuilderObject->setPayment($payment);
        $transactionBuilder->setOrder($order);
        $transactionBuilder->setTransactionId($transactionId);
        $transactionBuilder->setFailSafe(true);

        $paymentData = [];
        $paymentData = [
            'PayFast Original Transaction ID' => $originalTransactionId,
            'PayFast Refund Transaction ID' => $transactionId
        ];

        $transactionBuilder->setAdditionalInformation(
            [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
        );

        $transaction = $transactionBuilder->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_REFUND);
        $transaction->save();
    }
}
