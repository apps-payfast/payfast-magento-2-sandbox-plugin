<?php

namespace Apps\Payfast\Model\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Session;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\View\Element\Template\Context;

class PaymentMethodObserver implements ObserverInterface
{

    protected $checkoutSession;
    protected $orderFactory;
    protected $context;

    public function __construct(
        Context $context,
        Session $checkoutSession,
        OrderFactory $orderFactory
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->orderFactory = $orderFactory;
        $this->context = $context;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logger = $objectManager->create('\Apps\Payfast\Logger\Logger');

        $result          = $observer->getEvent()->getResult();
        $method_instance = $observer->getEvent()->getMethodInstance();
        $code = $method_instance->getCode();

        $shippingAddress = $this->checkoutSession->getQuote()->getShippingAddress();
        $orderShippingCountryId = $shippingAddress->getData('country_id');

        $config = $this->context->getScopeConfig();
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $allowedShippingCountryList = $config->getValue("payment/payfast/specific_shipping_country", $storeScope);

		$allowedShippingCountryArray = explode(",",$allowedShippingCountryList);
		$countryMatched = in_array($orderShippingCountryId, $allowedShippingCountryArray);
	

        if ($code == 'payfast' && !$countryMatched) {
            $logger->info("Hiding PayFast. Shipping Country is: ".$allowedShippingCountryList);
            $result->setData('is_available', false);
        }
    }
}
