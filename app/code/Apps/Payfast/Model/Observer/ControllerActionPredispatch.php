<?php

namespace Apps\Payfast\Model\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Session;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\View\Element\Template\Context;

# added this observer if javascript don't redirect to apps/redirect url.

class ControllerActionPredispatch implements ObserverInterface
{

    protected $checkoutSession;
    protected $orderFactory;
    protected $context;

    public function __construct(
        Context $context,
        Session $checkoutSession,
        OrderFactory $orderFactory
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->orderFactory = $orderFactory;
        $this->context = $context;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $request = $observer->getData('request');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logger = $objectManager->create('\Apps\Payfast\Logger\Logger');
        $logger->info("Module Name" . $request->getModuleName());
        $logger->info("Action Name" . $request->getActionName());

        if ($request->getModuleName() == "checkout" and $request->getActionName() == "success") {
            $orderId = $this->checkoutSession->getLastOrderId();
            if ($orderId) {
                $order = $this->orderFactory->create()->load($orderId);
                if ($order->getPayment()->getMethodInstance()->getCode() == "payfast" and $order->getState() == Order::STATE_NEW) {

                    $this->urlBuilder = \Magento\Framework\App\ObjectManager::getInstance()
                        ->get('Magento\Framework\UrlInterface');
                    $url = $this->urlBuilder->getUrl("payfast/redirect");
                    header("Location:$url");

                    exit;
                }
            }
        }
    }
}
