<?php

namespace Apps\Payfast\Model\PayFastAPI;

class PayFastApi
{
    private $_merchantId;
    private $_secureKey;
    private $_apiBaseUrl;
    private $_tokenEndpoint;
    private $_refundEndpoint;
    private $_error_message = null;
    private $_token;
    private $_logger;
    private $_refundTransactionId;

    public function __construct()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_logger = $objectManager->create('\Apps\Payfast\Logger\Logger');

        try {
            $this->_getAccessToken();
        } catch (\Magento\Framework\Exception\LocalizedException $exception) {
            throw ($exception);
        }
    }

    /**
     * call refund transaction request
     */
    public function refundTransaction($transactionId, $txnAmount, $storeCurrencyCode)
    {
        $refundCall = sprintf(
            "%s%s/%s",
            $this->_apiBaseUrl,
            $this->_refundEndpoint,
            $transactionId
        );

        if ((int)$txnAmount == $txnAmount) {
            $txnAmount = (int)$txnAmount;
        }
        

        $payload = [
            'txnamt' => $txnAmount,
            'refund_reason' => 'Order Refund',
            'currency_code' => $storeCurrencyCode
        ];

        $this->_logger->info("Transaction ID: " . $transactionId);
        $this->_logger->info("API Request: " . json_encode($payload));

        $postfields = http_build_query($payload);
        $responseObj = $this->__callRemoteRequest($refundCall, $postfields, 'POST', $httpcode);

        if (!$this->_validateJson($responseObj)) {
            $this->_error_message = 'Invalid response received from PayFast';
            return false;
        }

        $response = json_decode($responseObj);

        $this->_logger->info("API Response: " . $responseObj);

        if ($httpcode !== 200) {
            $this->_logger->info(sprintf("API Response: %s - %s", $response->code, $response->message));
            $message = $this->customMessage($response->code, $response->message);
            $this->_error_message = $message;
            throw new \Magento\Framework\Exception\LocalizedException(__($message));
        }

        if (isset($response->transaction_id)) {
            $this->_refundTransactionId = $response->transaction_id;
        }

        $this->_error_message = $response->message;
        return true;
    }

    /**
     * get API auth token
     */
    private function _getAccessToken()
    {
        $this->_getApiCredentials();

        $tokenCall = sprintf(
            "%s%s",
            $this->_apiBaseUrl,
            $this->_tokenEndpoint
        );

        $payload = [
            'merchant_id' => $this->_merchantId,
            'secured_key' => $this->_secureKey,
            'grant_type' => 'client_credentials'
        ];

        $postfields = http_build_query($payload);
        $responseObj = $this->__callRemoteRequest($tokenCall, $postfields, 'POST', $httpcode);


        if ($httpcode !== 200) {
            $this->_error_message = 'Token could not be fetched';
            throw new \Magento\Framework\Exception\LocalizedException(__($this->_error_message));
        }

        if (!$this->_validateJson($responseObj)) {
            $this->_error_message = 'Invalid token response received from PayFast';
            throw new \Magento\Framework\Exception\LocalizedException(__('API Authoriation Error. Invalid Response.'));
            return false;
        }

        $response = json_decode($responseObj);

        $this->_token = $response->token;
        $this->_logger->info("Auth token received.");
    }

    private function __callRemoteRequest($url, $fields, $method, &$httpcode)
    {
        $curlObject = curl_init();
        if ($method == 'POST') {
            curl_setopt($curlObject, CURLOPT_POST, 1);
            curl_setopt(
                $curlObject,
                CURLOPT_POSTFIELDS,
                $fields
            );
        }

        if ($method == 'GET') {
            if ($fields) {
                $url = sprintf("%s?%s", $url, $fields);
            }
        }

        $headerArray = array('Content-Type: application/x-www-form-urlencoded');

        if ($this->_token) {
            $headerArray = array_merge(
                $headerArray,
                [sprintf("Authorization: Bearer %s", $this->_token)]
            );
        }


        curl_setopt($curlObject, CURLOPT_URL, $url);
        curl_setopt($curlObject, CURLOPT_HTTPHEADER, $headerArray);
        curl_setopt($curlObject, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlObject, CURLOPT_USERAGENT, 'Magento Shopping Cart with PHP CURL');
        curl_setopt($curlObject, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlObject, CURLOPT_SSL_VERIFYPEER, 1);

        $response = curl_exec($curlObject);
        $httpcode = curl_getinfo($curlObject, CURLINFO_HTTP_CODE);
        curl_close($curlObject);
        return $response;
    }

    /**
     * get API credentials from Admin Config
     */
    private function _getApiCredentials()
    {
        $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();
        $configCollection = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
        $this->_merchantId =  $configCollection->getValue('payment/payfast/payfast_merchant_id');
        $this->_secureKey =  $configCollection->getValue('payment/payfast/payfast_secured_key');
        $this->_apiBaseUrl =  $configCollection->getValue('payment/payfast/payfast_api_url');
        $this->_refundEndpoint = $configCollection->getValue('payment/payfast/refund_api_endpoint');
        $this->_tokenEndpoint = $configCollection->getValue('payment/payfast/accesstoken_api_endpoint');
    }

    /**
     * validate JSON received remotely
     */
    private function _validateJson($string)
    {
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }

    /**
     * get error message
     */
    public function getError()
    {
        return $this->_error_message ? $this->_error_message : '';
    }

    public function getRefundTransactionId()
    {
        return $this->_refundTransactionId;
    }

    private function customMessage($code, $message)
    {

        $customMessage = [
            '13' => 'Partial refund for this transaction can only be perfomed after 24 hours from original transaction.'
        ];

        return isset($customMessage[$code]) ? $customMessage[$code] : $message;
    }
}
