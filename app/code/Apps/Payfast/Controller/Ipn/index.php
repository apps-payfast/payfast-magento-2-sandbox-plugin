<?php

namespace Apps\Payfast\Controller\Ipn;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Order;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Sales\Model\Order\Payment\Transaction\Builder as TransactionBuilder;

class Index extends \Magento\Framework\App\Action\Action implements CsrfAwareActionInterface
{
    protected $pageFactory;
    protected $transactionBuilder;
    protected $config;
    private $_paymentMethod = [
        'Account' => 'payfast_account',
        'Account Number' => 'payfast_account',
        'Wallet Number' => 'payfast_wallet',
        'Wallet' => 'payfast_wallet',
        'Card' => 'payfast_card',
        'UPCard' => 'payfast_upicard',
        'PayFast_Global' => 'payfast'
    ];

    public function __construct(
        Context $context,
        PageFactory $pageFactory
    ) {
        $this->pageFactory = $pageFactory;

        parent::__construct($context);
    }

    public function execute()
    {
        $basketId = $this->getRequest()->getParam('basket_id');
        $errorMsg = $this->getRequest()->getParam('err_msg');
        $errorCode = $this->getRequest()->getParam('err_code');
        $rdvMessageKey = $this->getRequest()->getParam('Rdv_Message_Key');
        $transactionId = $this->getRequest()->getParam('transaction_id');
        $redirect = $this->getRequest()->getParam('redirect');
        $txnPaymentName = $this->getRequest()->getParam('PaymentName');
        $validationHash = $this->getRequest()->getParam('validation_hash');
        $transactionId = !empty($transactionId) ? $transactionId : '';
        $discounted_amount = $this->getRequest()->getParam('discounted_amount');
        $transaction_amount = $this->getRequest()->getParam('transaction_amount');
        $merchant_amount = $this->getRequest()->getParam('merchant_amount');
        $transaction_currency = $this->getRequest()->getParam('transaction_currency');
        $post = $this->getRequest()->getPostValue();
        $txnPaymentName = $txnPaymentName ? $txnPaymentName : 'PayFast_Global';
        $payment_method = isset($this->_paymentMethod[$txnPaymentName]) ? $this->_paymentMethod[$txnPaymentName] : 'payfast';

        $ipnDate = date('Y-m-d H:i:s', time());

        $integrityCheck = $this->validateHash($validationHash, $basketId, $errorCode);
        if (!$integrityCheck) {
            if ($redirect == "Y") {
                $this->_redirect($this->urlBuilder->getUrl('checkout/cart', ['_secure' => true]));
            } else {
                echo 'Invalid Request';
            }
            return;
        }

        if (!$transactionId) {
            $transactionId = 'N/A - ' . $basketId;
        }


        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $collection = $objectManager->create('Magento\Sales\Model\Order');
        $order = $collection->loadByIncrementId($basketId);

        if (!$order) {
            echo "Order Not Found: " . $basketId;
            return;
        }


        $currentOrderState = $order->getState();

        if ($currentOrderState !== Order::STATE_NEW) {
            echo 'Order Already Updated: ' . $basketId;
            return;
        }


        $paymentData = [];
        $paymentData = [
            'PayFast Transaction ID' => $transactionId,
            'PayFast RDV Message Key' => $rdvMessageKey,
            'PayFast Error Code' => $errorCode,
            'PayFast Error Message' => $errorMsg,
            'Payment Instrument' => $txnPaymentName
        ];

        $payment = $order->getPayment();
        $invoice_error = '';
        $invoice = null;
        $payment->setMethod($payment_method);


        if ($errorCode == '000') {
            $order->setState(Order::STATE_PROCESSING)
                ->setStatus($order->getConfig()->getStateDefaultStatus(Order::STATE_PROCESSING));

            try {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $invoiceService = $objectManager->create('\Magento\Sales\Model\Service\InvoiceService');

                $invoice = $payment->getCreatedInvoice();

                if (!$invoice) {
                    $invoice = $invoiceService->prepareInvoice($order);
                    $invoice->setState(\Magento\Sales\Model\Order\Invoice::STATE_PAID);
                    $invoice_error = 'Invoice Created';
                    $invoice->setBaseGrandTotal($order->getBaseGrandTotal());
                    $invoice->register();
                    $invoice->getOrder()->setIsInProcess(true);
                    $invoice->pay();
                    $invoice->save();
                }
            } catch (Exception $e) {
                $invoice_error = $e->getMessage();
            }

            $order->setTotalPaid($order->getTotalPaid());
            $order->setBaseTotalPaid($order->getBaseTotalPaid());
        } 


        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $trans = $objectManager->create('\Magento\Sales\Model\Order\Payment\Transaction\Builder');

        $transactionBuilder = $trans->setPayment($payment);
        $transactionBuilder->setOrder($order);
        $transactionBuilder->setTransactionId($transactionId);
        $transactionBuilder->setFailSafe(true);
        $invoice ? $transactionBuilder->setSalesDocument($invoice) : '';
        $transactionBuilder->setAdditionalInformation(
            [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
        );

        //build method creates the transaction and returns the object
        $transaction = $transactionBuilder->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_PAYMENT);
        $transaction->save();

        if ($invoice) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $transFactory = $objectManager->create('\Magento\Framework\DB\TransactionFactory');
            $transObject = $transFactory->create()
                ->addObject($invoice)
                ->addObject($order);
            $transObject->save();
        }


        $payment->addTransactionCommentsToOrder(
            $transaction,
            'Invoice: ' . $invoice_error
        );


        $payment->addTransactionCommentsToOrder(
            $transaction,
            'Received via PayFast IPN at: ' . $ipnDate
        );

        $payment->addTransactionCommentsToOrder(
            $transaction,
            'PayFast Error Code: ' . $errorCode
        );

        $payment->addTransactionCommentsToOrder(
            $transaction,
            'PayFast Error Message: ' . $errorMsg
        );

        $payment->addTransactionCommentsToOrder(
            $transaction,
            'Payment Instrument: ' . $txnPaymentName
        );

        $payment->addTransactionCommentsToOrder(
            $transaction,
            'Original Merchant Amount: ' . $merchant_amount
        );

        $payment->addTransactionCommentsToOrder(
            $transaction,
            sprintf("Transaction Amount: %s %s", $transaction_currency, $transaction_amount)
        );


        if ($errorCode !== '000') {
            $payment->addTransactionCommentsToOrder(
                $transaction,
                "PayFast transaction was not successful."
            );
        } else {
            $payment->addTransactionCommentsToOrder(
                $transaction,
                "PayFast transaction is completed successfully."
            );

            $payment->addTransactionCommentsToOrder(
                $transaction,
                'PayFast Discounted Amount: ' . $discounted_amount
            );
        }

        $order->setCanSendNewEmailFlag(true);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $objectManager->create('Magento\Sales\Model\OrderNotifier')->notify($order);

        $order->save();
        $payment->save();

        if ($errorCode === '000') {
            echo  'Order Updated with Successfull Transaction';
        } else {
            echo  'Order Updated with Failed Transaction';
        }
        return;
    }

    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }

    private function validateHash($validation_hash, $order_id, $err_code)
    {
        $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();
        $configCollection = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
        $merchantId =  $configCollection->getValue('payment/payfast/payfast_merchant_id');
        $secretKey =  $configCollection->getValue('payment/payfast/payfast_secured_key');

        $protocol = sprintf(
            "%s|%s|%s|%s",
            $order_id,
            $secretKey,
            $merchantId,
            $err_code
        );

        $hash = hash('sha256', $protocol);
        return $hash == $validation_hash;
    }
}
