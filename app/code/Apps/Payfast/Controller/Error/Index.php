<?php

namespace Apps\Payfast\Controller\Error;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Session;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;


class Index extends \Magento\Framework\App\Action\Action
{

    protected $pageFactory;
    protected $checkoutSession;
    protected $orderFactory;

    public function __construct(Session $checkoutSession, OrderFactory $orderFactory, Context $context, PageFactory $pageFactory)
    {
        $this->pageFactory = $pageFactory;

        $this->checkoutSession = $checkoutSession;
        $this->orderFactory = $orderFactory;

        parent::__construct($context);
    }

    public function execute()
    {


        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $messageManager = $objectManager->create('\Magento\Framework\Message\ManagerInterface');
        $messageManager->addWarning(__("Provided shipping address is not allowed for this payment method. Please use different payment method."));
        return $this->pageFactory->create();
    }
}
