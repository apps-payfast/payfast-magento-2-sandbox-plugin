## PayFast Payment Gateway for Magento 2

Enable PayFast as a payment method in Magento 2

How to Install:
==============

- Extract the .zip package in Magento 2 Root
- Enable the module in Magento Module Manager
- Go to Magento Admin/Configurations/Sales/Payment Method and Select Payfast and provide the necessary configurations

Features:
========

- segregation for payment method chosen at PayFast by customer.
- Invoice creation on successfull payment.
- Online refund.
- IPN support.


Developed by Tech Team

Tested on Magento ver. 2.4.2

https://gopayfast.com

info@gopayfast.com

Last Updated: 16-Aug-2022